from graphics import GraphWin, Image, Point

def main():
    win = GraphWin("Dubrownik 2012", 960, 720)
    win.setCoords(-480, -360, 480, 360)
    image = Image(Point(0, 0), "obraz.png")
    image.draw(win)
    win.getMouse()
    win.close()

main()
