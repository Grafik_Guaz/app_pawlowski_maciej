from graphics import GraphWin, Point, Text

def main():
    win = GraphWin("My Circle", 960, 600)
    win.setCoords(-400, -300, 400, 300)
    t = Text(Point(0, 0), "Nihil Novi Sub Sole")
    t.draw(win);  win.getMouse()
    t.setTextColor("blue"); win.getMouse()
    t.setSize(36); win.getMouse()
    t.setStyle("bold"); win.getMouse()
    t.setStyle("bold italic"); win.getMouse()
    win.close()

if __name__ == "__main__":
    main()

