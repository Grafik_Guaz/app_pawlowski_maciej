from tkinter import *
from tkinter import Tk

class Main_Tk():

    def __init__(self):
        def update():
            self.text_field.set("Witaj " + self.text_field.get() + "!!!")
        root = Tk()
        root.geometry("200x200")
        frame = Frame(root)
        self.text_field = StringVar()
        lbl = Entry(frame, textvariable=self.text_field)
        lbl.pack()
        btt = Button(frame, text="Wykonaj", command=lambda: update())
        btt.pack()
        frame.pack()
        root.mainloop()

Main_Tk()