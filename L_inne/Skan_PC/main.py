from parametr_cpu import parameters_cpu as pc_cpu
from pdf_gen import gen_pdf as g_pdf
from codabar import codabar as cdb
from datetime import datetime as dt
from os import system as cmd
from tkinter import *
from tkinter import messagebox
from tkinter import PhotoImage

class main_window():
    def __init__(self):
        self.main()
        root = Tk()
        self.os_frame = Frame(root)

        #Screen size
        self.screen_width = root.winfo_screenwidth()
        self.screen_height = root.winfo_screenheight()
        #print('Screen resolution: ', self.screen_width, 'x', self.screen_height)

        self.os_header = Frame(root)
        self.os_tail = Frame(root)
        self.now_is = dt.now()
        self.window(root)

        def main_close_window():
            if messagebox.askokcancel("Zamknij", "Jestes pewien ze chcesz zamknac program?"):
                exit('Program zostal pomyslnie zamkniety.')

        root.protocol("WM_DELETE_WINDOW", main_close_window)

        def update():
            self.tkimg = PhotoImage(file="draw-img.gif")
            self.tkimg_label.config(image=self.tkimg)
            root.after(1000, update)

        root.after(5000, update)
        root.mainloop()


    def main(self):
        pc_cpu()
        pass

    def window(self, root):
        root.wm_title('CPU reader v1.0')
        root.geometry("600x600")
        self.header_frame_create()
        self.body_frame_create()
        self.tail_frame_create()
        self.os_header.pack(side=TOP, fill=Y)
        self.os_tail.pack(side=BOTTOM, fill=BOTH)
        self.os_frame.pack(side=TOP, fill=Y)

    def header_frame_create(self):
        check_time = Label(self.os_header, text="Aktualny czas: {}".format(self.now_is.strftime("%H:%M %d.%m.%Y")))
        check_time.grid(row=0)

    def body_frame_create(self):
        #Comment widget
        comment_label = Label(self.os_frame, text="Wstaw komentarz: ")
        comment_label.grid(row=1,column=1)

        self.comment_entry_var = StringVar()
        comment_entry = Text(self.os_frame)
        comment_entry.grid(row=2, column=1, columnspan=4, sticky=E)

        #Enter coda ID
        entry_label= Label(self.os_frame, text="Wprowadz recznie ID \n(codabar): ")
        entry_label.grid(row=5, column=1, sticky=NSEW)

        self.coda_entry = StringVar()
        entry_coda = Entry(self.os_frame, textvariable=self.coda_entry)
        entry_coda.grid(row=5, column=2, columnspan=3)
        self.coda_entry.set("*opcjonalnie")

    def tail_frame_create(self):
        #Button functions
        czytaj = Button(self.os_tail, text="Ukonczono sprawdzanie")
        czytaj.bind("<Button-1>", self.confirm)
        czytaj.pack(side=BOTTOM, fill=X)

        blank = Button(self.os_tail, text="Wyswietl bialy ekran")
        blank.bind("<Button-1>", func=self.blank_screen)
        blank.pack(side=BOTTOM, fill=X)

        coda = Button(self.os_tail, text="Generuj nowy codabar")
        coda.bind("<Button-1>", func=self.new_coda)
        coda.pack(side=BOTTOM, fill=X)

        # ZCZYTAJ CODA PRZEZ SSH
        try:
            self.tkimg = PhotoImage(file="draw-img.gif")
        except:
            cdb('0000')
            self.tkimg = PhotoImage(file="draw-img.gif")
        self.tkimg_label = Label(self.os_tail, image=self.tkimg)
        self.tkimg_label.image = self.tkimg
        self.tkimg_label.pack(side=BOTTOM, fill=BOTH)

        #Check variables
        self.check_A = IntVar()
        self.check_B = IntVar()
        self.check_Bp = IntVar()
        self.check_C = IntVar()

        raportA = Checkbutton(self.os_tail, text="A", variable=self.check_A)
        raportA.pack(side=LEFT, fill=Y)

        raportB = Checkbutton(self.os_tail, text="B", variable=self.check_B)
        raportB.pack(side=LEFT, fill=Y)

        raportBp = Checkbutton(self.os_tail, text="B+", variable=self.check_Bp)
        raportBp.pack(side=LEFT, fill=BOTH)

        raportC = Checkbutton(self.os_tail, text="C", variable=self.check_C)
        raportC.pack(side=LEFT, fill=BOTH)

    def confirm(self, event):
        if self.check_A.get():
          self.class_check='A'
        elif self.check_B.get():
          self.class_check='B'
        elif self.check_Bp.get():
          self.class_check='B+'
        elif self.check_C.get():
          self.class_check='C'
        if not self.check_A.get()+self.check_B.get()+self.check_Bp.get()+self.check_C.get() == 1:
            messagebox.showwarning('Błąd!', 'Zaznacz prawidlowo klase sprzetu.')
            return 0
        sec_root = Tk()
        sec_root.wm_title('Check confirm')
        os_confirm = Frame(sec_root)

        warning_frame = Label(os_confirm, text="Jestes pewny wprowadzonych danych?")
        warning_frame.grid(row=0, columnspan=2)

        confirm_button = Button(os_confirm, text="Tak\nWydrukuj etykiete.")
        confirm_button.bind("<Button-1>", self.printing_raport)
        confirm_button.grid(row=1, column=0)

        abort_button = Button(os_confirm, text="Nie\nPowroc do okna", command=sec_root.destroy)
        abort_button.grid(row=1, column=1)

        os_confirm.pack()
        sec_root.mainloop()


    def printing_raport(self, event):
        g_pdf(self.class_check)
        cmd('sudo lp -d plik.pdf')
        pass

    def new_coda(self, event):
        Check_ID = str(self.coda_entry.get())
        print(Check_ID)
        if not Check_ID == "*opcjonalnie" and not Check_ID == "":
            try:
                cdb(Check_ID)
            except TypeError:
                messagebox.showwarning('Niedozwolone znaki',
                                       'Wprowadziles niedozwolone znaki, kod kreskowy nie zostal zmieniony.')

    def blank_screen(self, event):
        #cmd('metacity --replace')
        sec_root = Tk()
        sec_root.wm_title('Blank Screen')
        sec_root.configure(background="white")
        sec_root.geometry(str(self.screen_width+200) + 'x' + str(self.screen_height+200))
        sec_root.bind("<Escape>", lambda i: sec_root.destroy())
        sec_root.bind("<Button-3>", lambda i: sec_root.destroy())
        os_fill = Frame(sec_root)
        os_fill.pack(fill=BOTH)
        messagebox.showwarning('Zamykanie', 'By zamknac bialy ekran, wcisnij Escape lub kliknij prawym przyciskiem myszy.')
        sec_root.mainloop()

    def do_nothing(self, event):
        print("Tchorzliwie odmawiam wykonania niedokonczonej operacji!")


if __name__ == "__main__":
    main_window()
