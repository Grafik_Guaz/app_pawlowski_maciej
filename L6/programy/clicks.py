from graphics import GraphWin

def main():
    win = GraphWin("Kliknij mnie 6 razy!", 800, 600)
    for j in range(6):
        p = win.getMouse()
        print("Kliknąłeś w punkcie: ", end = "") 
        print(p.getX(), p.getY())

if __name__ == "__main__":
    main()
