from graphics import GraphWin, Point, Entry

def main():
    win = GraphWin("Pole wprowadzania", 960, 600)
    win.setCoords(-480, -300, 480, 300)
    e = Entry(Point(0, 0), 40)
    e.setSize(24); e.setTextColor("red");
    e.setStyle("bold"); e.setText(""); 
    e.draw(win);
    win.getMouse()
    print(e.getText())
    win.close()

if __name__ == "__main__":
    main()

