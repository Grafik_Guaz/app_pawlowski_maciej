class Licznik:
    wartosc = 0

    def zwieksz(self):
        self.wartosc += 1

    def zmniejsz(self):
        self.wartosc -= 1

    def get_wartosc(self):
        return self.wartosc

def main():
    zmienna = Licznik()
    print(zmienna.get_wartosc())
    zmienna.zmniejsz()
    print(zmienna.get_wartosc())
    zmienna.zmniejsz()
    print(zmienna.get_wartosc())
    zmienna.zwieksz()
    print(zmienna.get_wartosc())

if __name__=="__main__":
    main()