class Human:
    def __init__(self, name='Zenosuaw', surname='Burczymucha',
                 gender='kobieta', age=22, height=186):
        self.name = name
        self.surname = surname
        self.gender = gender
        self.age = age
        self.height = height
        self.group = {"imie": self.name, "nazwisko": self.surname, "plec": self.gender, "wiek": self.age, "wzrost": self.height}

    def __repr__(self):
        return self

    def __str__(self):
        return "Imię: " + str(self.group.get("imie")) + "\nNazwisko: " + str(self.group.get("nazwisko")) + \
               "\nPłeć: " + str(self.group.get("plec")) + "\nWiek: " + str(self.group.get("wiek")) + \
               "\nWzrost: " + str(self.group.get("wzrost")) +"\n"

    def __add__(self, arg):
        try:
            if len(arg) == 2:
                self.group["wzrost"] += int(arg[1])
                self.group["wiek"] += int(arg[0])
                self.height += int(arg[1])
                self.age += int(arg[0])
            else:
                raise TypeError
        except TypeError:
            print("Enter tuple (age_modyfication, height_modyfication.")
        except IndexError:
            print("If u not modyficate value, write instead null.")
        except Exception as other_error:
            print(other_error)

    def __sub__(self, arg):
        try:
            if len(arg) == 2:
                self.group["wzrost"]-= int(arg[1])
                self.group["wiek"]-= int(arg[0])
                self.height -= int(arg[1])
                self.age -= int(arg[0])
            else:
                raise TypeError
        except TypeError:
            print("Enter tuple (age_modyfication, height_modyfication.")
        except IndexError:
            print("If u not modyficate value, write instead null.")
        except Exception as other_error:
            print(other_error)



def main():
    zmienna = Human(name="Ziemniak", surname="Burkowski", gender="Mężczyzna", age=28, height=192)
    print(zmienna)
    zmienna+3
    print(zmienna)
    zmienna-(2,20)
    print(zmienna)
    zmienna+(1,5)
    print(zmienna)
    print(zmienna.name)
    print(zmienna.surname)
    print(zmienna.gender)
    print(zmienna.age)
    print(zmienna.height)


if __name__ == "__main__":
    main()
