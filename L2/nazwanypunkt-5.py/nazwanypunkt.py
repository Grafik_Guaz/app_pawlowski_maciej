from punkt import Punkt

class NazwanyPunkt(Punkt):
	ile = 0
	def __init__(self, x, y, nazwa):
		Punkt.__init__(self, x, y)
		self.__nazwa = nazwa
		NazwanyPunkt.ile + 1

	def __str__(self):
		return (self.__nazwa + "<" + str(self.x)
			+ ", " + str(self.y) + ">")

	@property
	def nazwa(self):
		return self.__nazwa

	@nazwa.setter
	def nazwa(self, val):
		try:
			self.__nazwa = str(val)
		except:
			raise TypeError("Argument musi byc ciagiem znakow!")

	@nazwa.deleter
	def nazwa(self):
		return TypeError("Nie mozna usunac nazwy!")
