#Maciej Pawłowski

def body():
	print ("Pamiętaj, ilość kolumn macierzy A, musi być równa ilości wierszy macierzy B!")
	print ("Liczby w wierszu, podawaj jako ciąg cyfr niczym nieoddzielony.")
	macierz_a=[[g for g in list(int(j) for j in list(input("Liczby w wierszu {0}: ".format(i+1))))] for i in range(int(input("Wiersze macierzu A: ")))]
	macierz_b=[[g for g in list(int(j) for j in list(input("Liczby w wierszu {0}: ".format(i+1))))] for i in range(int(input("Wiersze macierzu B: ")))]
	transformuj= lambda g: [[new[i] for new in g] for i in range(len(g))]
	macierz_bt=transformuj(macierz_b)
	print ("Macierz A", macierz_a)
	print ("Macierz B", macierz_b)
	print ("Macierz B obrócona", macierz_bt)
	input()
	iloczyn(macierz_a,macierz_b)

def iloczyn(a,b):
#Zmienne tymczasowe
	ln_a, ln_b=len(a), len(b)
	wyzszy=max(ln_a, ln_b)
	szerszy=max(len(b[0]),len(a[0]))

#Stwórz listę zerową:
	c=[[0]*szerszy for i in range(wyzszy)]

#Oblicz wyrazy:
	for i in range(ln_a):
		for j in range(len(b[0])):
			for g in range(ln_b):
				c[i][j] = c[i][j] + a[i][g] * b[g][j]

#Wyświetl działanie:
	print (c)
	print ("Mnożenie macierzy!\n{0:11} X {1:11} = {2:11}".format("Macierz A", "Macierz B", "Macierz C"))
	for i in range(wyzszy+1):
		if ln_a>i and ln_b>i:
			print("{0:11} X {1:11} = {2:11}".format(str(a[i]), str(b[i]), str(c[i])))
		elif ln_a<=i and ln_b>i:
			print("{0:11} X {1:11} = {2:11}".format("", str(b[i]), str(c[i])))
		elif ln_b<=i and ln_a>i:
			print("{0:11} X {1:11} = {2:11}".format(str(a[i]), "", str(c[i])))
		else:
			break
	return c

body()

